<?php
/**
 * Template Name: Inner Page 1
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package mosa
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php
			while ( have_posts() ) : the_post();
				$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );	 ?>
				<div class="banner-inner" style="background-image: url(<?php echo $featured_image[0]; ?>)">
					<div class="container-fluid">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</div>
				</div>
				<div class="breadcrumbs">
					<div class="container-fluid">
						<?php $ancestors = get_post_ancestors( $post ); 
						array_reverse($ancestors);
						?>
						 <ul class="breadcrumb-list list-inline list-unstyled">
						 <?php 
						 	foreach ($ancestors as $key => $ancestor) { ?>
						 	<li><a href="<?php echo get_permalink($ancestor); ?>"><?php echo get_the_title($ancestor); ?></a></li>
						 <?php }
						 ?>
						 	<li class="current"><?php echo the_title(); ?></li>
						 </ul>
					</div>
				</div>
				<div class="container-fluid">
					<?php if ( is_page() && $post->post_parent ) { 
						$args = array(
							'sort_order' => 'asc',
							'sort_column' => 'post_title',
							'child_of' => get_the_ID(),
							'parent' => get_the_ID(),
							'hierarchical' => 0,
							'post_type' => 'page',
							'post_status' => 'publish'
						); 
						$pages = get_pages($args);
					?>
					<h2 class="side-nav-title"><?php echo get_the_title( wp_get_post_parent_id( get_the_ID() ) );?></h2>
					<div class="row">
							<?php if ( is_page() && $post->post_parent && sizeof($pages)) { ?>
						<div class="col-xs-12 col-sm-2 pull-right ">
							
							<ul class="side-nav-list">
								<?php foreach ($pages as $page) {
									echo "<li><a href=".get_permalink($page->ID).">$page->post_title</a></li>";
								} ?>
							</ul>
						</div>
							<?php  wp_reset_postdata();} }?>			
						<div class="col-xs-12 col-sm-10 pull-right">
							<div class="mosa-inner-wrap">
								<?php the_content();?>
							</div><!-- .mosa-inner-wrap -->
						</div>
					</div>	
					<div class="mos-an-wrap row">
						 <div class="col-xs-6 col-sm-4 col-md-3">
						 	<img src="<?php echo get_template_directory_uri()?>/images/an-1.jpg"/>
						 </div><!-- .col -->
						 <div class="col-xs-6 col-sm-4 col-md-3">
						 	<span class="an-dummy"></span>
						 </div><!-- .col -->
						  <div class="col-xs-6 col-sm-4 col-md-3">
						 	<span class="an-dummy"></span>
						 </div><!-- .col -->
						  <div class="col-xs-6 col-sm-4 col-md-3">
						 	<span class="an-dummy"></span>
						 </div><!-- .col -->
					</div><!-- .row -->				
				</div>
			<?php endwhile; // End of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
