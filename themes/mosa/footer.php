<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mosa
 */

?>

	</div><!-- #content -->
	
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="wrap-footer-nav">
			<div class="container-fluid">
				<div class="footer-nav-item">				
					<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-1', 'menu_id' => 'footer-menu-1', 'menu_class' => 'footer-nav list-unstyled list-inline' ) ); ?>
				</div>
				<div class="footer-nav-item">				
					<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-2', 'menu_id' => 'footer-menu-2', 'menu_class' => 'footer-nav list-unstyled list-inline' ) ); ?>
				</div>
				<div class="footer-nav-item">				
					<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-3', 'menu_id' => 'footer-menu-3', 'menu_class' => 'footer-nav list-unstyled list-inline' ) ); ?>
				</div>
				<div class="footer-nav-item">				
					<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-4', 'menu_id' => 'footer-menu-4', 'menu_class' => 'footer-nav list-unstyled list-inline' ) ); ?>
				</div>
				<div class="footer-nav-item">				
					<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-5', 'menu_id' => 'footer-menu-5', 'menu_class' => 'footer-nav list-unstyled list-inline' ) ); ?>
				</div>	
				<div class="footer-contact">
					<?php dynamic_sidebar( 'footer-contact' ); ?>
				</div>	
			</div><!-- .container-fluid -->
		</div><!-- .wrap-footer-nav -->
		<div class="container-fluid">
			<div class="rights">جميع الحقوق محفوظة لوزارة الشؤون الرياضية</div>
		</div><!-- .container-fluid -->
	</footer><!-- #colophon -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
