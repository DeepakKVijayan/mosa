<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mosa
 */

?><!DOCTYPE html>
<html <?php //language_attributes(); ?> lang="ar" dir="rtl">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="site-header" role="banner">
	<div class="container-fluid header-inner">
		<a class="site-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
		<nav id="site-navigation" class="main-navigation" role="navigation">
			<a href="#" id="trigger-overlay" class="hamburger">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
				<span></span>
				<span></span>
	        </a>
			<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?>
			<?php wp_nav_menu( array( 'theme_location' => 'menu-2', 'menu_id' => 'secondary-menu' ) ); ?>
		</nav><!-- #site-navigation -->
	</div><!-- .container-fluid -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
