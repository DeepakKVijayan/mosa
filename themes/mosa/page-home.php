<?php
/**
 * Template Name: Home
 *
 * @package mosa
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php  $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );	
				while ( have_posts() ) : the_post();
					$banner_text = get_field('banner_text');
			?>
			<div id="bannerCarousel" class="carousel slide" data-ride="carousel"> 
		        <!-- Wrapper for carousel items -->
		        <div class="carousel-inner">
					<?php if( have_rows('banner_slider') ): ?>
						<?php $countbi = 1; while ( have_rows('banner_slider') ) : the_row(); 
							$carouselImage = get_sub_field('image');
						?>
							<div class="item banner <?php if($countbi == 1) echo 'active';?>" style="background-image: url('<?php echo $carouselImage['url'] ?>');">
				                <!-- <img src="<?php echo $carouselImage['url']; ?>" alt="mosa"> -->
				                <h2><?php echo get_sub_field('text');?></h2>
				            </div>
						<?php $countbi++; endwhile; ?>
					<?php endif; ?>
		        </div>
		        <!-- Carousel controls -->
		        <a class="carousel-control left" href="#bannerCarousel" data-slide="prev">
		            <span class="glyphicon glyphicon-chevron-left banner-slider-control"></span>
		        </a>
		        <a class="carousel-control right" href="#bannerCarousel" data-slide="next">
		            <span class="glyphicon glyphicon-chevron-right banner-slider-control"></span>
		        </a>
		    </div><!-- .carousel -->

			<!-- <div class="banner" style="background-image: url('<?php echo $featured_image[0] ?>');">
				<?php echo the_content(); ?>
			</div> --><!-- .banner -->
			<div class="announcement-section">
				<div class="container-fluid">
					<div class="row announcement-inner">
						<div class="col-xs-12 col-sm-10 news-wrap">
							<h2>أحدث الأخبار</h2>
							<?php $post_object = get_field('news');
								if( $post_object ): 
									$post = $post_object;
									setup_postdata( $post ); 
									?>
								    <div class="news-item row">
										<div class="col-xs-12 col-sm-2 hidden-xs"></div>
										<div class="col-xs-12 col-sm-3 col-sm-push-7">
											<?php echo get_the_post_thumbnail( $post->ID, 'medium' ); ?>
								    	</div>
										<div class="col-xs-12 col-sm-7 col-sm-pull-3">
											<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									    	<span class="news-date"><?php echo get_field('date'); ?></span>
									    	<p><?php echo get_field('short_description'); ?></p>
									    	<a class="read-more" href="<?php echo get_permalink($post->ID); ?>">المزيد</a>
										</div>
								    </div>
								    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
								<?php endif; ?>
						</div><!-- .col -->
						<div class="col-xs-12 col-sm-2 announcement-wrap">
							<h2>الاعلان</h2>
							<img src="<?php echo get_field('announcement'); ?>" />
						</div><!-- .col -->
					</div><!-- .row -->
				</div><!-- .container-fluid -->
			</div><!-- .announcement-section -->
			<div class="announcement-sub-section">
				<div class="container-fluid">
					<p>أقيم اليوم بديوان عام الوزارة حفل بمناسبة العيد الوطني 46 بحضور معالي الشيخ الوزير       في عمومية اتحاد الكرة الطائرة..تزكية الرواس رئيسا للفترة 2016-2020م </p>
				</div>
			</div><!-- .container-fluid -->
			<div class="tabs-section">
				<div class="container-fluid">
					<ul class="category-list">
						<?php
						if( have_rows('tabs') ):
							$count = 1;
							while ( have_rows('tabs') ) : the_row(); ?>
								<li class="<?php if($count == 1) echo "active"; ?>">
									<span><?php echo get_sub_field('title'); ?></span>
									<a href="#<?php echo $count; ?>a" data-toggle="tab" style="background-image: url('<?php echo get_sub_field('image'); ?>')">
									</a>
								</li>
							<?php $count++;
							endwhile;
						endif;
						?>
					</ul>
				</div><!-- .container-fluid -->
			</div><!-- .tabs-section -->
			<div class="tabs-section-content">
				<div class="container-fluid">
					<div class="tab-content clearfix">
						<?php if( have_rows('tabs') ):
							$countc = 1;
							while ( have_rows('tabs') ) : the_row(); ?>
								<ul class="tab-pane <?php if($countc == 1) echo "active"; ?> tab-logos-wrap" id="<?php echo $countc; ?>a">
									<?php 
									if( have_rows('logos') ):
										while ( have_rows('logos') ) : the_row(); 
										$logo_image = get_sub_field('logo_image');
									?>
						    				<li><img src="<?php echo $logo_image['url']; ?>" alt="mosa"></li>		
										<?php endwhile;
									endif; ?>
								</ul><!-- .tab-pane -->
								<?php $countc++;
							endwhile;
						endif; ?>
					</div><!-- .tab-content -->
				</div><!-- .container-fluid -->
			</div><!-- .tabs-section -->
			<div class="photo-gallery-section">
				<div class="container-fluid">
					<?php if( have_rows('photo_gallery') ): ?>
					<h2><?php echo get_field('photo_gallery_title'); ?></h2>
					<ul class="slider-list">
						<?php $countpg = 1;  while ( have_rows('photo_gallery') ) : the_row(); ?>
							<li class="<?php if($countpg == 1) echo "active"; ?>"><a data-toggle="tab" href="#<?php echo $countpg; ?>p"><?php echo get_sub_field('title'); ?></a></li>
						<?php $countpg++; endwhile; ?>
					</ul>
					<?php endif; ?>
					<?php if( have_rows('photo_gallery') ): ?>
						<div class="tab-content clearfix">
						<?php $countpgc = 1; while ( have_rows('photo_gallery') ) : the_row(); ?>
							<div id="<?php echo $countpgc; ?>p" class="tab-pane <?php if($countpgc == 1) echo "active"; ?>">							
								<div id="myCarousel<?php echo $countpgc; ?>" class="carousel slide" data-ride="carousel"> 
							        <!-- Wrapper for carousel items -->
							        <div class="carousel-inner">
										<?php if( have_rows('slider') ): ?>
											<?php $counti = 1; while ( have_rows('slider') ) : the_row(); 
												$slider_image = get_sub_field('images');
											?>
												<div class="item <?php if($counti == 1) echo 'active';?>">
									                <img src="<?php echo $slider_image['url']; ?>" alt="mosa">
									            </div>
											<?php $counti++; endwhile; ?>
										<?php endif; ?>
							        </div>
							        <!-- Carousel controls -->
							        <a class="carousel-control left" href="#myCarousel<?php echo $countpgc; ?>" data-slide="prev">
							            <span class="glyphicon glyphicon-chevron-left"></span>
							        </a>
							        <a class="carousel-control right" href="#myCarousel<?php echo $countpgc; ?>" data-slide="next">
							            <span class="glyphicon glyphicon-chevron-right"></span>
							        </a>
							    </div><!-- .carousel -->
							</div><!-- .tab-pane -->
						<?php $countpgc++; endwhile; ?>
						</div><!-- .tab-content -->
					<?php endif; ?>
				</div><!-- .container-fluid -->
			</div><!-- .photo-gallery-section -->
			<div class="sitemap">
				<div class="container-fluid">
					<p class="sitemap-title">خريطة الموقع</p>
				</div><!-- .container-fluid -->
			</div><!-- .site-map -->
			<?php endwhile; // End of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
